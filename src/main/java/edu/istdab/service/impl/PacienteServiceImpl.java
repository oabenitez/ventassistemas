package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Persona;
import edu.istdab.repo.IPersonaRepo;
import edu.istdab.service.IPersonaService;

@Service
public class PacienteServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaRepo repo;
	

	@Override
	public List<Persona> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}


	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

	@Override
	public Persona registrar(Persona obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Persona modificar(Persona obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Persona leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Persona> op= repo.findById(id);
		return op.isPresent() ? op.get() : new Persona();
	}

}
