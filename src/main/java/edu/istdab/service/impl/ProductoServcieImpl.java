package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Producto;
import edu.istdab.repo.IProductoRepo;
import edu.istdab.service.IProductoService;

@Service
public class ProductoServcieImpl implements IProductoService{

	@Autowired
	private IProductoRepo repo;

	@Override
	public List<Producto> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}


	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

	@Override
	public Producto registrar(Producto obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Producto modificar(Producto obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Producto leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Producto> esp=repo.findById(id);
		return esp.isPresent() ? esp.get() : new Producto();
	}

}
