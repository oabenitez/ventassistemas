package edu.istdab.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.NOT_FOUND)
public class ModeloNotFoudException extends RuntimeException{

	//constructor
	public ModeloNotFoudException (String mensaje){
		super(mensaje);
	}
}
