package edu.istdab.exception;

import java.time.LocalDateTime;



public class ExceptionResponse {

	private LocalDateTime timeSamp;
	private String mensaje;
	private String detalle;
	
	public ExceptionResponse(LocalDateTime timeSamp, String mensaje, String detalle) {
		super();
		this.timeSamp = timeSamp;
		this.mensaje = mensaje;
		this.detalle = detalle;
	}
	
	public LocalDateTime getTimeSamp() {
		return timeSamp;
	}
	public void setTimeSamp(LocalDateTime timeSamp) {
		this.timeSamp = timeSamp;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	
}
