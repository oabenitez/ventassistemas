package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Persona;


public interface IPersonaRepo extends JpaRepository<Persona, Integer> {
	

}
