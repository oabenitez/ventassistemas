package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Producto;

public interface IProductoRepo extends JpaRepository<Producto, Integer> {

}
