package edu.istdab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentasSistemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentasSistemaApplication.class, args);
	}

}
